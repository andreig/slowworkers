#!/usr/bin/env python
from collections import defaultdict
from datetime import datetime, timedelta
import os, re
import time, sys

#2021-11-01T23:24:45.877 6a19f309-7e04-4a9a-8d5f-e817ddf38235 POST /user/login - - - frontend16
# -- front time --
#2021-11-01T23:24:45.895 6a19f309-7e04-4a9a-8d5f-e817ddf38235 HANDLE - - - - worker16
# -- worker time --
#2021-11-01T23:24:45.927 6a19f309-7e04-4a9a-8d5f-e817ddf38235 RESPOND - - 200 - frontend16


# TODO to make it faster
# - replace strptime which is slow with parsed date since we know the format
# - parallelize to read chunks and pass it to worker nodes to parse

IN_FILE = "sample.log"
if not os.path.isfile(IN_FILE):
    sys.exit(IN_FILE + " not found, run getSample.py to get one")

# format is missing miliseconds intentionally, splitting them off to cache date parsing 
# then adding them back at compute time
DATE_FORMAT = "%Y-%m-%dT%H:%M:%S"

if sys.platform == "win32":
    timer = time.clock
else:
    timer = time.time
    
# timer to count processing performance time
t0 = timer()

# bunch of hashes to cache / aggregate data 
guid_map = defaultdict(int) # stores a server with a time tuple (server, timestamp) by trace, consequent traces diff from "timestamp"
worker_time = defaultdict(int) # stores total work time for each server, #TODO could be a tuple with worked sessions ?
worker_sessions = defaultdict(int) # stores worked sessions
parsed_dates = defaultdict(str) #caches already parsed dates without miliseconds to speed up parsing

for line in open(IN_FILE, "rb"):
    parts = line.split(" ")
    log_time = parts[0]
    log_trace = parts[1]
    log_server = parts[-1]

    # split time to cache repetitive entries eg. 2021-11-01T23:24:45 N times
    (log_time_main, log_time_mili) = log_time.split(".")
    if not log_time_main in parsed_dates:
        parsed_dates[log_time_main] = datetime.strptime(log_time_main, DATE_FORMAT)

    time_process = parsed_dates[log_time_main]
    time_process += timedelta(microseconds=float(log_time_mili)*1000)

    if not log_trace in guid_map:
        guid_map[log_trace] = (log_server, time_process)
    else:
        worker_time[guid_map[log_trace][0]] += (time_process - guid_map[log_trace][1]).total_seconds() * 1000
        guid_map[log_trace] = (log_server, time_process)
        worker_sessions[guid_map[log_trace][0]] += 1

# average work times
for key in worker_time:
    worker_time[key] = worker_time[key] / worker_sessions[key]

print timer() - t0

# sanity check
for key in sorted(worker_time, key=worker_time.get)[-10:]:
    print "%20s = %s" % (key.rstrip(), worker_time[key])
