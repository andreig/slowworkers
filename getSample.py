#!/usr/bin/env python
import urllib, os

if not os.path.isfile("sample.log"):
    urllib.urlretrieve("https://s3.eu-central-1.amazonaws.com/cs0.openbsd-box.org/logfiles/sample.log", "sample.log")
