# Sample log parsing
Parsing a sample.log efficiently for slow worker detection

Goals: parse as fast as possible using as little memmory as possible

I worked on two versions, used Python 2.7 and 3 to toy around a bit,  
would have been more comfortable writing this in Perl or Java probably,  
you gotta miss [autovivification](https://en.wikipedia.org/wiki/Autovivification) from Perl

* slowWorkers.py - initial Python 2.7 version, toyed with regex but that's def. slower than string slicing
* 3slowWorkers.py -  uses a faster time parsing routine

Code needs some modularizing love, variables renamed, comments refined, cleaned up 

## Problem description
We have a server farm arranged such that our front-facing servers (that handle the raw HTTP traffic) 
and our backend servers (that do the actual response generation) are separated. We have 20 front-end 
HTTP servers, and 50 worker servers that generate the actual responses. Our problem is that some of 
these front-end and worker servers are responding slower than others. 
We need to figure out which servers are the culprits.

Included is a combined log file of all front-end and worker servers. 
The log file has a fairly simple format with the following space separated fields:

`datestamp, request GUID, action type, requested url, -, status, -, server/worker id`

A typical transaction will look something like this:

```
2013-06-10T09:46:37.330 6300c7d4-dec5-43cf-af65-f9700745d33b GET /card/comments - - - frontend19
2013-06-10T09:46:37.490 6300c7d4-dec5-43cf-af65-f9700745d33b HANDLE - - - - worker36
2013-06-10T09:46:37.600 6300c7d4-dec5-43cf-af65-f9700745d33b RESPOND - - 200 - frontend19
```

A successful solution will be performant over large data sets and return the complete 
list of slow worker/front-end servers.

Please respond to the email that invited you to this technical exercise with a list of slow worker/front-end  
servers and attach the code.  Please include any instructions necessary for us to compile and run the code.

## Setup & run
In order to download the sample log file run

`python getSample.py`

This will download the `sample.log` file from an S3 bucket

Run with `python3 3slowWorkers.py` or `python slowWorkers.py` if you only have pyt2.7 installed

## Log structure & logic

Assuming `-- front time --` and `-- worker time --` are calculated like indicated below.  
Also assuming that a front will work with multiple workers. 
  
  
```
2021-11-01T23:24:45.877 6a19f309-7e04-4a9a-8d5f-e817ddf38235 POST /user/login - - - frontend16
# -- front time --
2021-11-01T23:24:45.895 6a19f309-7e04-4a9a-8d5f-e817ddf38235 HANDLE - - - - worker16
# -- worker time --
2021-11-01T23:24:45.927 6a19f309-7e04-4a9a-8d5f-e817ddf38235 RESPOND - - 200 - frontend16
```

## Logic

Script parses line by line, tokenizes / extracts interesting bits, parses timestamps  
(noticed native strptime library is painfully slow in python 2.7 so added caching),  
traces GUID jobs and records workers, as it goes through lines it also calculates time differences 
like front time, worker time and accumulates number of worked jobs and total work time.  
After parsing the file it averages worked times, sorts results and prints the slowest workers.


* read line by line, fetch timestamp, guid and server
* split timestamp between date/time and miliseconds
* cache timestamp date/time to prevent parsing the same chunk over and over
* transform timestamp to miliseconds, reconciling with the miliseconds chopped earlier
* if we see a GUID the first time, register it in a dict and log time time it started
* any other subsequent hits on the GUID is a  diff time between current timestamp and last occured timestamp, accumulate worker time and # of worked sessions 
* last calculate avg, sort and print results

## Sample output

1st line is processing ellapsed time, next lines are listing bottom performing workers


```
andrei@PHZQ001:~/play/git/wildbit$ python3 3slowWorkers.py 
2.277138792909682
             worker5 = 50.31075
            worker20 = 50.40175
             worker1 = 50.418
            worker50 = 50.4235
            worker11 = 50.58725
           frontend3 = 732.3895
           frontend8 = 732.7551
            worker47 = 748.60875
            worker30 = 750.34625
            worker33 = 750.737

```

## Performace notes

Parsing large data-sets means loading a lot of data into memory, to avoid that one needs to stream
and calculate / keep track of times while going over the file. Loading it into memory only to aggregate
and calculate at a later stage will not be possible for, let's say 50Gb files. Initially used the default P2.7  
interpreter and played a bit with regexes, profiling time. Then I iterated over with the goal to bring down time  
of parsing to under 3 seconds (arbitrary goal).


#### Python2.7
 v1 uses cached strptime ~ 3.0sec / run
#### Python3
 v1 uses cached fromisofirmat ~ 2.4sec / run

### Improvements / speed up ideas 

 [X] cache time parsing  
 [X] replace strptime which is slow with parsed date since we know the format  
 [ ] maybe parse timestamp manually ? I doubt it would be faster than fromiso in python3  
 [ ] parallelize file reads to get chunks and pass them to worker threads and reconcile the worker tiem totals and job totals  
 [ ] ... and feed it into ELK or datadog :P 

